# ItxTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.1.

## Development server

To run the project you have to run `npm install` and then `ng serve` in the console and navigate to `http://localhost:4200/` and the currency app will load

This application reads three values from a form, the amount and type of currency and and the currency types that you want to consult, use the api `https://www.frankfurter.app/` to send the information entered, and returns the worth of the amount selected, in the currencies indicated.

To se the app published enter to `https://61b4e6571fcd5e2096276ab5--stupefied-bell-a1b8cb.netlify.app/`
