import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { CurrencyService } from './service/currency.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Currency exchange';
  currencyExchangeForm: FormGroup = this.formBuilder.group({
    fromCurrencyValue: ['', Validators.required],
    fromCurrencyCoin: ['', Validators.required],
    toCurrencyCoin: ['', Validators.required],
  });
  currenciesArray: Array<any> = [];
  conversionList: Array<any> = [];
  isFormValid: boolean = true;
  _subs: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private currencyService: CurrencyService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.currencyService.getCurrencies().subscribe({
      next: (currencyResponse: any) => {
        const currencyKeys = Object.keys(currencyResponse);
        const currencyValues = Object.values<string>(currencyResponse);
        for (let i = 0; i < currencyKeys.length; i++) {
          const newCurrencyItem = {
            id: currencyKeys[i],
            name: currencyValues[i],
          };
          this.currenciesArray.push(newCurrencyItem);
        }
      },
      error: (error) => {
        this.openSnackBar(
          'We are sorry, there has been an error retrieving the currencies information, try again later'
        );
      },
    });
    this._subs.add(
      this.currencyExchangeForm.valueChanges.subscribe((change: any) => {
        const { fromCurrencyValue } = change;
        if (fromCurrencyValue < 1) {
          this.currencyExchangeForm.controls['fromCurrencyValue'].setValue(1);
        }
      })
    );
  }

  /**
   * Function to get the currency name, takes as params
   * the currencyId and returns the name
   * @param currencyId 
   * @returns currencyName
   */
  getCurrencyName(currencyId: string) {
    if (this.currenciesArray.length !== 0) {
      return this.currenciesArray.find((item: any) => item.id === currencyId)
        .name;
    } else {
      return '';
    }
  }

  /**
   * Event function from form, validates the form, and do the request to get
   * the conversion
   */
  convert() {
    this.getFormValidation();
    if (this.currencyExchangeForm.valid) {
      const amount =
        this.currencyExchangeForm.controls['fromCurrencyValue'].value;
      const from = this.currencyExchangeForm.controls['fromCurrencyCoin'].value;
      let to = '';
      this.currencyExchangeForm.controls['toCurrencyCoin'].value.forEach(
        (currencyId: string, index: number) => {
          if (index !== 0) {
            to += `,`;
          }
          to += `${currencyId}`;
        }
      );
      const currencyParams = {
        amount,
        from,
        to,
      };
      this.currencyService.getCurrencyConversion(currencyParams).subscribe({
        next: (conversionResponse: any) => {
          if (conversionResponse) {
            const rateKeys = Object.keys(conversionResponse.rates);
            const rateValues = Object.values<string>(conversionResponse.rates);
            this.conversionList = [];
            for (let i = 0; i < rateKeys.length; i++) {
              this.conversionList.push({
                id: rateKeys[i],
                value: rateValues[i],
                name: this.getCurrencyName(rateKeys[i]),
              });
            }
          }
        },
        error: (error) => {
          this.openSnackBar(
            'We are sorry, there has been an error converting the currencies, try again later'
          );
        },
      });
    }
  }

  /**
   * Function to open the snack bar, takes as params
   * the msg
   * @param msg 
   */
  openSnackBar(msg: string) {
    this.snackBar.open(msg, 'Ok', {
      duration: 5000,
      verticalPosition: 'bottom',
      horizontalPosition: 'right',
    });
  }

  /**
   * Function to validate if the user pushes and the form
   * is invalid show an error msg
   */
  getFormValidation() {
    this.isFormValid = !(
      this.currencyExchangeForm.controls['fromCurrencyValue'].value === '' ||
      this.currencyExchangeForm.controls['fromCurrencyCoin'].value === '' ||
      this.currencyExchangeForm.controls['toCurrencyCoin'].value === ''
    );
  }

  /**
   * Unsubscribe of subscriptions
   */
  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }
}
