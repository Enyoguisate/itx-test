import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CurrencyService {
  apiUrl = 'https://api.frankfurter.app';
  currenciesUrl = '/currencies';
  latestCurrency = '/latest';

  constructor(private httpClient: HttpClient) {}

  getCurrencies() {
    return this.httpClient.get(`${this.apiUrl}${this.currenciesUrl}`);
  }

  getCurrencyConversion(currencyParams: any) {
    const { amount, from, to } = currencyParams;
    let params = new HttpParams()
      .set('amount', amount)
      .set('from', from)
      .set('to', to);
    return this.httpClient.get(`${this.apiUrl}${this.latestCurrency}`, {
      params
    });
  }
}
